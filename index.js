const commander = require('commander')
const lodash = require('lodash')
const utils = require('./lib/utils')

async function main () {
  const program = commander.program
    .requiredOption('--baseurl <url>', 'Mobileapps API endpoint')
    .requiredOption('--dataset-path <path>', 'Articles to be checked')
    .option('--concurrency <integer>', 'Number of concurrent tests', parseInt, 0)
    .parse()

  const opts = program.opts()
  const titles = utils.datasetReader(opts.datasetPath)
  const chunks = lodash.chunk(titles, opts.concurrency)

  for (const chunk of chunks) {
    const partial = lodash.partial(utils.testArticle, opts.baseurl)
    const tests = lodash.map(chunk, partial)
    let results = await Promise.all(tests)
    console.log(results);
  }
}

main()
