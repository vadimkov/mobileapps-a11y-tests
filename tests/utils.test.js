const mocha = require('mocha')
const utils = require('../lib/utils')
const assert = require('assert')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

mocha.it('returns the default URL', () => {
  assert.equal(
    utils.getPageURL('http://example.com', 'foo'),
    'http://example.com/foo'
  )
})

mocha.it('returns the dark theme URL', () => {
  assert.equal(
    utils.getPageURL('http://example.com', 'foo', true),
    'http://example.com/foo?theme=dark'
  )
})

mocha.it('runs the a11y tests with same issues', async () => {
  const stub = sinon.stub()
  const returnedIssues = {
    issues: [
      { code: 1, selector: 1 },
      { code: 2, selector: 2 }
    ]
  }
  stub.returns(returnedIssues)

  const mockedUtils = proxyquire('../lib/utils', {
    pa11y: stub
  })

  const result = await mockedUtils.testArticle('http://example.com', 'foo')
  const expected = {
    hasDiffIssues: false,
    resultsDefaultTheme: returnedIssues,
    resultsDarkTheme: returnedIssues
  }
  assert.deepEqual(result, expected)
})

mocha.it('runs the a11y tests with different issues', async () => {
  const stub = sinon.stub()
  const firstIssues = {
    issues: [
      { code: 1, selector: 1 },
      { code: 2, selector: 2 }
    ]
  }
  const secondIssues = {
    issues: [
      { code: 3, selector: 3 },
      { code: 4, selector: 4 }
    ]
  }
  stub.onFirstCall().returns(firstIssues)
  stub.onSecondCall().returns(secondIssues)

  const mockedUtils = proxyquire('../lib/utils', {
    pa11y: stub
  })

  const result = await mockedUtils.testArticle('http://example.com', 'foo')
  const expected = {
    hasDiffIssues: true,
    resultsDefaultTheme: firstIssues,
    resultsDarkTheme: secondIssues
  }
  assert.deepEqual(result, expected)
})

mocha.it('reads dataset from path', () => {
  const expected = { query: { search: [{ title: 'foo' }, { title: 'bar' }] } }
  const mockedUtils = proxyquire('../lib/utils', {
    fs: {
      readFileSync: (datasetPath) => JSON.stringify(expected)
    }
  })

  assert.deepEqual(mockedUtils.datasetReader('/bogus/path'), ['foo', 'bar'])
})
