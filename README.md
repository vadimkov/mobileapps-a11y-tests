# Accessibility tests for mobileapps API

Current scope is to test for dark theme rendering issues on mobileapps API but it can be expanded to more accessibility tests.

## Installation

```bash
npm install
```

## Usage

```bash
node index.js --help
```

### Examples

Run the script

```bash
node index.js \
    --baseurl "https://en.wikipedia.org/api/rest_v1/page/mobile-html" \
    --dataset-path <path> \
    --concurrency 1
```

Example MW API call for articles with specific template:
```bash
https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch=hastemplate:Infobox+baseball+biography
```

Example dataset

```json
{
  "batchcomplete": "",
  "continue": {
    "sroffset": 500,
    "continue": "-||"
  },
  "query": {
    "searchinfo": {
      "totalhits": 14886
    },
    "search": [
      {
        "ns": 0,
        "title": "Bridgerton",
        "pageid": 62811365,
        "size": 97108,
        "wordcount": 5624,
        "snippet": "Bridgerton is an American streaming television period drama series created by Chris Van Dusen and produced by Shonda Rhimes. It is based on Julia Quinn's",
        "timestamp": "2022-04-07T03:44:23Z"
      },
      {
        "ns": 0,
        "title": "Moon Knight (TV series)",
        "pageid": 61593083,
        "size": 99352,
        "wordcount": 7570,
        "snippet": "Moon Knight is an American television miniseries created by Jeremy Slater for the streaming service Disney+, based on the Marvel Comics featuring the character",
        "timestamp": "2022-04-07T07:57:50Z"
      }
    ]
  }
}
```
