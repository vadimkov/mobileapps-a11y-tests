const pa11y = require('pa11y')
const fs = require('fs')
const lodash = require('lodash')

const A11YRULES = ['Principle1.Guideline1_4.1_4_3_Contrast']

function getPageURL (baseURL, article, darkTheme = false) {
  const url = new URL(baseURL)
  const articleURL = new URL(article, url)

  if (darkTheme) {
    articleURL.searchParams.set('theme', 'dark')
  }

  return articleURL.toString()
}

async function testArticle (baseURL, article) {
  const config = {
    rules: A11YRULES
  }
  const resultsDefaultTheme = await pa11y(getPageURL(baseURL, article), config)
  const resultsDarkTheme = await pa11y(
    getPageURL(baseURL, article, true),
    config
  )

  const uniqueDefault = lodash.map(
    resultsDefaultTheme.issues,
    lodash.partialRight(lodash.pick, ['code', 'selector'])
  )

  const uniqueDark = lodash.map(
    resultsDarkTheme.issues,
    lodash.partialRight(lodash.pick, ['code', 'selector'])
  )

  return {
    hasDiffIssues: !lodash.isEqual(uniqueDark, uniqueDefault),
    resultsDefaultTheme,
    resultsDarkTheme
  }
}

function datasetReader (datasetPath) {
  const data = JSON.parse(fs.readFileSync(datasetPath))
  return lodash.map(
    data.query.search,
    (elem) => elem.title
  )
}

exports.testArticle = testArticle
exports.datasetReader = datasetReader
exports.getPageURL = getPageURL
